# Sciences de l'Ingénieur

*Stéphane Ramstein : <stephane.ramstein@ac-lille.fr>*

*Enseignant d'Informatique et de Physique-Chimie au lycée Raymond Queneau de Villeneuve d'Ascq*

--------------------


Lien vers le dépôt GitLab des ressources :

* [Dépôt GitLab](https://gitlab.com/stephane_ramstein/si/-/tree/main/docs?ref_type=heads)

Lien vers la page web image du dépôt :

* [Page web du dépôt](https://stephane_ramstein.gitlab.io/si)


--------------------

Cette année nous étudierons le processeur et sa programmation.


## Le processeur, une machine binaire

Le processeur (ou unité centrale de calcul, UCC, en anglais central processing unit, CPU) est un composant présent dans de nombreux dispositifs électroniques qui exécute les instructions machine des programmes informatiques. Il permet à la machine dans laquelle il est implanté de fonctionner de manière autonome et de traiter les données très rapidement et en grande quantité.

- Qu'est-ce qu'un ordinateur ? : [pdf](./0001_processeur/processeur_et_binaire.pdf) ou [odt](./0001_processeur/processeur_et_binaire.odt)
    - Tableau des nombres de 0 à 255 en base 2, 10 et 16 : [pdf](./0001_processeur/fiche_nombre_bases.pdf) ou [odt](./0001_processeur/fiche_nombre_bases.ods)

- Les entiers en base 2 : [pdf](./0001_processeur/base_2.pdf) ou [odt](./0001_processeur/base_2.odt)


## Opérateurs et fonction logiques

En binaire les opérations de base ne sont pas les additions, soustractions, multiplications et divisions, mais des opérations NON, ET, OU, acceptant les valeurs VRAI ou FAUX et restituant une réponse du même type. Elles se réalisent à l'aide de circuit intégrés composés de transistors microscopiques actionnés automatiquement par des signaux électriques. Un microprocesseur en contient des millions. Il fonctionne donc de manière logique et ne peut manipuler les bits qu'avec ces opérations de base. En combinant les opérateurs logiques, il est possible de recréer les fonctions plus complexes telles que l'addition, la soustraction, la multiplication et la division. Les mathématiciens ont démontré que cet algèbre et les circuits logiques peuvent résoudre tous les problèmes qui ont une solution.

- Les opérations en binaire : [pdf](./0001_processeur/operations_binaires.pdf) ou [odt](./0001_processeur/operations_binaires.odt)


- Interrupteurs et opérateurs logiques : [pdf](./0001_processeur/interrupteurs_et_operateurs_logiques.pdf) ou [odt](./0001_processeur/interrupteurs_et_operateurs_logiques.odt)


## Calcul mécanique et algorithmes

Les algorithmes modernes de calcul reposent sur une longue accumulation d'idées mathématiques et de mises en oeuvres techniques dont le but est d'automatiser le calcul et notamment les opérations de base : addition, soustraction, multiplication et addition. Il ne faudrait pas croire que ce domaine des sciences est ancien et que l'Homme le maîtrise entièrement. En effet l'ordinateur est une invention récente et le dernier algorithme de multiplication pour les très grands entiers a été trouvé en 2007 par le mathématicien suisse Martin Fürer. Les enjeux sont très importants puisque par exemple la sécurisation de données est liée à la rapidité des machines à les crypter et les décrypter à l'aide d'algorithmes de calcul.

- TP sur le calcul mécanique et les algorithmes de calcul : [pdf](./03_calcul_mecanique/TP_calcul_mecanique_algorithmes.pdf) ou [odt](./03_calcul_mecanique/TP_calcul_mecanique_algorithmes.odt)
    - Vidéo de démonstration de l'utilisation du soroban japonais : [émission Japan in Motion](./03_calcul_mecanique/Demonstration_du_soroban_japonais.mp4)
    - Exemple de bâtons de Neper : [pdf](./03_calcul_mecanique/Batons_de_Neper.pdf) ou [ods](./03_calcul_mecanique/Batons_de_Neper.ods)


--------------------

# Outils

- Initiation au dessin technique sur onshape : [pdf](./outils/initiation_on_shape.pdf) ou [odt](./outils/initiation_on_shape.odp)