
## Prise en main du matériel

- Le Raspberry Pi et la programmation Python : [pdf](./05_Raspberry/03a_prise_en_main_Raspberry.pdf) ou [odt](./05_Raspberry/03a_prise_en_main_Raspberry.odt)
    - Fiche sur l'utilisation du Grove Base Hat avec CAN permettant d'utiliser le port GPIO plus facilement: [pdf](./05_Raspberry/03e_Fiche_eleve_Grove_Base_Hat.pdf) ou [odt](./05_Raspberry/03e_Fiche_eleve_Grove_Base_Hat.odt)
    - Fiche sur l'utilisation du port GPIO avec Python: [pdf](./05_Raspberry/03b_Fiche_eleve_Raspberry_GPIO.pdf) ou [odt](./05_Raspberry/03b_Fiche_eleve_Raspberry_GPIO.odt)
    - Fiche sur l'utilisation avancée du port GPIO avec Python: [pdf](./05_Raspberry/03c_Fiche_eleve_Raspberry_GPIO_complements.pdf) ou [odt](./05_Raspberry/03c_Fiche_eleve_Raspberry_GPIO_complements.odt)


## Un capteur : la thermistance

- Première approche : [pdf](./02_un_capteur/la_thermistance_sans_etalonnage.pdf) ou [odt](./02_un_capteur/la_thermistance_sans_etalonnage.odt)
- Conversion résistance/tension : [pdf](./03_conversion_resistance_tension/pont_diviseur_autre_version.pdf) ou [odt](./03_conversion_resistance_tension/pont_diviseur_autre_version.odt)
- Conversion analogique/numérique : [pdf](./07_CAN_Raspberry/06_CAN_sous_Raspberry.pdf) ou [odt](./07_CAN_Raspberry/06_CAN_sous_Raspberry.odt)
    - modules python pour l'utilisation du CAN du Raspberry Pi Hat : [zip](./07_CAN_Raspberry/ADC.zip)